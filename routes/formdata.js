const express = require("express")
const router = express.Router()

const auth = require("../middleware/auth")
const {
	createFormData,
	getFormDataByType,
	getAllFormData,
	getFormData,
	approveFormData,
	rejectFormData,
} = require("../controllers/formDataController")
const { subAdminAuth } = require("../middleware/adminAuth")

router.post("/", auth, createFormData)

router.get("/:id", getFormDataByType)

router.get("/", auth, subAdminAuth, getAllFormData)

router.get("/:id", auth, subAdminAuth, getFormData)

router.post("/:id", auth, subAdminAuth, approveFormData)

router.put("/:id", auth, subAdminAuth, rejectFormData)

module.exports = router
