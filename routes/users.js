const express = require("express")
const router = express.Router()
const {
	authentication,
	getAllUsers,
	createAdmin,
	getUser,
	removeAdmin,
} = require("../controllers/userController")
const auth = require("../middleware/auth")
const { adminAuth } = require("../middleware/adminAuth")

// const Forms=require("../models/Forms");

router.post("/", authentication)
router.get("/users", auth, adminAuth, getAllUsers)
router.post("/:email", auth, adminAuth, createAdmin)
router.put("/:email", auth, adminAuth, removeAdmin)
router.get("/:email", auth, adminAuth, getUser)

module.exports = router
