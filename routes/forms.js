const express = require("express")
const router = express.Router()

const auth = require("../middleware/auth")
const {
	getFormByType,
	types,
	getAllForms,
	getForm,
	updateForm,
	deleteForm,
	createForm,
} = require("../controllers/formsController")
const { subAdminAuth } = require("../middleware/adminAuth")

router.get("/types", types)
router.get("/:type", getFormByType)
router.get("/", auth, subAdminAuth, getAllForms)
router.post("/:id", auth, subAdminAuth, getForm)
router.put("/:id", auth, subAdminAuth, updateForm)
router.delete("/:id", auth, subAdminAuth, deleteForm)
router.post("/", auth, subAdminAuth, createForm)

module.exports = router
