import React, { useState, useEffect } from "react"
import Form from "react-jsonschema-form"
import Spinner from "../pages/Spinner"
import axios from "axios"

const Select = (props) => {
	const [value, setValue] = useState("Individual_kyc")
	const [schema, setSchema] = useState(null)
	const [flag, setFlag] = useState(false)
	const [types, setTypes] = useState([])

	//send alert message to parent component
	const alertMessage = (msg) => {
		props.passMessage(msg)
	}

	//dropdown submit action
	const handleSubmit = async (e) => {
		e.preventDefault()

		try {
			const formschema = await axios
				.get(`/api/forms/${value}`)
				.then((res) => res.data.formschema)

			setSchema(formschema)

			setFlag(true)
		} catch (err) {
			alertMessage(err)
		}
	}

	useEffect(() => {
		const fetchTypes = async () => {
			const typeArray = await axios
				.get(`/api/forms/types`)
				.then((res) => res.data)
			setTypes(typeArray)
		}
		fetchTypes()
	}, [])

	const onChange = (e) => {
		setValue(e.target.value)
	}

	//kyc form submission action
	const onSubmit = async ({ formData }) => {
		try {
			const payload = {
				type: value,
				formdata: formData,
			}
			const config = {
				header: {
					"Content-Type": "application/json",
				},
			}
			const result = await axios.post("/api/formdata", payload, config)

			props.history.push({
				pathname: "/success",
				state: {
					id: result.data._id ? result.data._id : result.data.id,
				},
			})
		} catch (err) {
			alertMessage(err)
		}
	}

	//render components conditionally
	if (!flag && !schema) {
		return (
			<div className="form-container">
				<form onSubmit={handleSubmit}>
					Select an option:
					<select
						value={value}
						onChange={onChange}
						style={{ width: "100%", height: "2.5rem" }}
					>
						{types.map((type, index) => (
							<option key={index} value={`${type}`}>
								{type}
							</option>
						))}
						{/* // <option value="Organization_kyc">Organization KYC</option>} */}
					</select>
					<input type="submit" value="Submit" />
				</form>
			</div>
		)
	} else if (schema && flag) {
		return (
			<div>
				<Form
					className="form-container"
					schema={schema}
					onSubmit={onSubmit}
				></Form>
			</div>
		)
	} else {
		return <Spinner></Spinner>
	}
}
export default Select
