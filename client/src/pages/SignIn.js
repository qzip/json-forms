import axios from "axios"
import React, { useState } from "react"

import "../styles/mongoLogin.css"

const SignIn = (props) => {
	const [name, setName] = useState("")
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [sign, setSign] = useState("in")

	const handleSignUp = async () => {
		const data = await axios
			.post("/api/auth/signup", {
				name,
				email,
				password,
			})
			.then((res) => res.data)

		if (data.status !== "fail") {
			localStorage.setItem("token", data.token)
			props.history.push("/select")
		}
	}

	const handleSignIn = async () => {
		const data = await axios
			.post("/api/auth/signin", {
				email,
				password,
			})
			.then((res) => res.data)

		if (data.status !== "fail") {
			localStorage.setItem("token", data.token)
			props.history.push("/select")
		}
	}

	if (sign === "in") {
		return (
			<>
				<div className="card">
					<h3>Email</h3>
					<input
						type="email"
						onChange={(e) => setEmail(e.target.value)}
						value={email}
					/>
					<h3>Password</h3>
					<input
						type="password"
						onChange={(e) => setPassword(e.target.value)}
						value={password}
					/>
					<button className="button" onClick={handleSignIn}>
						Submit
					</button>
					<h4 onClick={() => setSign("up")}>
						Sign Up if you do not have an account.
					</h4>
				</div>
			</>
		)
	} else if (sign === "up") {
		return (
			<>
				<div className="card">
					<h3>Name</h3>
					<input
						type="text"
						onChange={(e) => setName(e.target.value)}
						value={name}
					/>
					<h3>Email</h3>
					<input
						type="email"
						onChange={(e) => setEmail(e.target.value)}
						value={email}
					/>
					<h3>Password</h3>
					<input
						type="password"
						onChange={(e) => setPassword(e.target.value)}
						value={password}
					/>
					<button className="button" onClick={handleSignUp}>
						Submit
					</button>
					<h4 onClick={() => setSign("in")}>Sign In if you have an account.</h4>
				</div>
			</>
		)
	}
}

export default SignIn
