import React, { useState, useEffect, Fragment } from 'react';

const Alerts = ({message}) => {
    console.log(message);
    let [visibility ,setVisibility]=useState(false);
    useEffect(()=> {
        if(message) {
            console.log(message);
            setVisibility(true);
            setTimeout(() => {
                setVisibility(false);
            }, 5000);
        }
        // eslint-disable-next-line
    }, [message]);

return(
    <Fragment>
        {visibility && (
            <div className="alert alert-danger">
                <i className="fas fa-info-circle">{" "}{message}</i>
            </div>
        )}
    </Fragment>
)
    
}  

export default Alerts;