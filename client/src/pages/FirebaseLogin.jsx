import React, { useState, Fragment } from "react"
import "react-phone-number-input/style.css"
import PhoneInput from "react-phone-number-input"
import firebase from "../firebase"
import axios from "axios"
import Spinner from "./Spinner"
import setAuthToken from "../Utils/setAuthToken"

const FirebaseLogin = (props) => {
	const [number, setNumber] = useState("")
	const [code, setCode] = useState("")
	const [otpsent, setOtpsent] = useState(false)

	//send alert message to parent component to render alert component
	const alertMessage = (msg) => {
		props.passMessage(msg)
	}
	var recaptchaVerifier = null

	//set up invisibe recaptcha
	const setUpRecaptcha = () => {
		recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
			"recaptcha-container",
			{
				size: "invisible",
				callback: function (response) {
					console.log("Captcha Resolved")
					onSubmit1()
				},
			}
		)
	}

	//add otp value
	const onChange = (e) => {
		setCode(e.target.value)
	}
	//after submission of phone number
	const onSubmit1 = (e) => {
		e.preventDefault()
		setUpRecaptcha()
		let phoneNumber = number
		let appVerifier = recaptchaVerifier
		//verify user by number and code entered
		firebase
			.auth()
			.signInWithPhoneNumber(phoneNumber, appVerifier)
			.then(function (confirmationResult) {
				window.confirmationResult = confirmationResult
				setOtpsent(true)
				alertMessage("OTP sent...")
			})
			.catch(function (error) {
				alertMessage(
					"Couldn't send OTP, please check the number provided. \nRefresh the page and try again"
				)
			})
	}
	// after submission of otp code
	const onSubmit2 = (e) => {
		e.preventDefault()
		let otpInput = code
		let otpConfirm = window.confirmationResult
		otpConfirm
			.confirm(otpInput)
			.then(async (result) => {
				try {
					const payload = {
						number,
						code,
					}
					const config = {
						header: {
							"Content-Type": "application/json",
						},
					}
					const res = await axios.post("/api/user", payload, config)
					await localStorage.setItem("token", res.data.token)
					//load token into header
					if (localStorage.token) {
						setAuthToken(localStorage.token)
					}
					props.history.push("/select")
				} catch (err) {
					alertMessage(err.message)
				}
			})
			.catch(function (error) {
				alertMessage(error)
			})
	}
	//conditionally rendering components
	if (!otpsent) {
		return (
			<Fragment>
				<div className="form-container">
					<form onSubmit={onSubmit1}>
						<label htmlFor="number">
							<h3>Enter Your Phone Number here: </h3>
						</label>
						<PhoneInput
							defaultCountry="IN"
							placeholder="Enter phone number"
							value={number}
							name="number"
							onChange={setNumber}
						/>
						<div id="recaptcha-container"></div>
						<button
							type="submit"
							name="submit"
							value="Submit"
							disabled={!number}
						>
							Submit
						</button>
					</form>
				</div>
			</Fragment>
		)
	} else if (otpsent) {
		return (
			<Fragment>
				<div className="form-container">
					<form onSubmit={onSubmit2}>
						<label htmlFor="code">
							<h3>Enter the Code received on the given number here:</h3>
						</label>
						<input
							type="text"
							name="code"
							value={code}
							onChange={onChange}
						></input>
						<input
							type="submit"
							name="submit"
							value="Verify"
							disabled={code.length < 6}
						></input>
					</form>
				</div>
			</Fragment>
		)
	} else {
		return <Spinner></Spinner>
	}
}
export default FirebaseLogin
