import React, { useEffect, useState } from "react"
import * as ReactBootStrap from "react-bootstrap"
import axios from "axios"
import Spinner from "./Spinner"

const Display = (props) => {
	//send alert message to parent component
	const alertMessage = (msg) => {
		props.passMessage(msg)
	}
	//receive id of the entered document

	const id = Object.values(props.history.location.state)[0]
	const [form, setForm] = useState(null)
	useEffect(() => {
		const fetchdata = async () => {
			try {
				const res = await axios.get(`/api/formdata/${id}`)

				setForm(res.data.formdata)
			} catch (err) {
				alertMessage(err.message)
			}
		}
		fetchdata()
		// eslint-disable-next-line
	}, [])

	// display form
	if (form) {
		return (
			<div className="display-container">
				<div>
					<div className="alert alert-success">
						<i className="fas fa-check-circle"></i> Successfully added the
						details
					</div>
				</div>
				<div className="container">
					<ReactBootStrap.Table>
						<tbody>
							{Object.keys(form).map((key) => (
								<tr key={key}>
									<td>{key.charAt(0).toUpperCase() + key.slice(1)}</td>
									{key === "file" ? (
										<td>
											<img
												src={form[key]}
												style={{ width: "55%", height: "70%" }}
												alt="file data"
											></img>
										</td>
									) : (
										<td>{form[key]}</td>
									)}
								</tr>
							))}
						</tbody>
					</ReactBootStrap.Table>
				</div>
			</div>
		)
	}
	return <Spinner></Spinner>
}
export default Display
