import React from "react"
import FirebaseLogin from "./FirebaseLogin"
import SignIn from "./SignIn"

const Verify = (props) => {
	if (process.env.REACT_APP_DATABASE === "1") return <FirebaseLogin />
	if (process.env.REACT_APP_DATABASE === "2") return <SignIn />
}

export default Verify
