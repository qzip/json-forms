import * as firebase from "firebase"

var firebaseConfig = {
	apiKey: process.env.REACT_APP_FIREBASE_API_KEY, // USE YOUR OWN API KEY :)
	authDomain: "https://immense-hollows-74804.herokuapp.com",
	databaseURL: process.env.REACT_APP_DATABSE_URL, // USE YOUR OWN DATABASE URL
	projectId: process.env.REACT_APP_PROJECT_ID, // YOUR PROJECT ID
	storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
	messagingSenderId: "507919283495",
	appId: "1:507919283495:web:ce95b249bb76c26bbc6502",
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

export default firebase
