import React, { Fragment, useState } from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Select from "./pages/Select"
import Display from "./pages/Display"
import Alerts from "./pages/Alerts"
import Navbar from "./pages/Navbar"
import "./App.css"
import setAuthToken from "./Utils/setAuthToken"
import SignIn from "./pages/SignIn"
import FirebaseLogin from "./pages/FirebaseLogin"

// load the current user for the session
if (localStorage.token) {
	setAuthToken(localStorage.token)
}
//parent component
const App = () => {
	const [message, setMessage] = useState("")
	const passMessage = (msg) => {
		setMessage(msg)
	}

	return (
		<Router>
			<Fragment>
				<Navbar></Navbar>
				<div className="container">
					<Alerts message={message}></Alerts>
					<Switch>
						<Route
							exact
							path="/"
							render={(props) =>
								process.env.REACT_APP_DATABASE === "1" ? (
									<FirebaseLogin {...props} passMessage={passMessage} />
								) : (
									<SignIn {...props} passMessage={passMessage} />
								)
							}
						/>

						<Route
							exact
							path="/select"
							render={(props) => (
								<Select {...props} passMessage={passMessage} />
							)}
						/>
						<Route
							exact
							path="/success"
							render={(props) => (
								<Display {...props} passMessage={passMessage} />
							)}
						></Route>
					</Switch>
				</div>
			</Fragment>
		</Router>
	)
}

export default App
