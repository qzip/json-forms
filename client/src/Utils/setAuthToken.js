import axios from 'axios';

//set token in the axios global header
const setAuthToken=(token)=> {
    if(token) {
        axios.defaults.headers.common["x-auth-token"]=token;
    } else {
        delete axios.defaults.headers.common["x-auth-tokens"];
    }
}

export default setAuthToken;