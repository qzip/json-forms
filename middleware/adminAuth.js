const expressAsyncHandler = require("express-async-handler")
const { FindUser } = require("../databaseAdapters/FindAdapter")

exports.adminAuth = expressAsyncHandler(async (req, res, next) => {
	const user = await FindUser(req.user)

	if (user.isAdmin !== 3) {
		throw Error
	}
	next()
})

exports.subAdminAuth = expressAsyncHandler(async (req, res, next) => {
	const user = await FindUser(req.user)

	if (user.isAdmin === 3) {
		next()
	} else if (user.isAdmin === 2) {
		next()
	} else {
		throw Error
	}
})
