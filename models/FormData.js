const mongoose = require("mongoose")
const DataSchema = mongoose.Schema({
	type: {
		type: String,
		required: true,
	},
	formdata: {
		type: Object,
		required: true,
	},
	date: {
		type: Date,
		default: Date.now,
	},
	approved: {
		type: Number,
		default: 0,
	},
})

module.exports = mongoose.model("data", DataSchema)
