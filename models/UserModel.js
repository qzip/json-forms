const mongoose = require("mongoose")
const userSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	email: {
		type: Object,
		required: true,
	},
	hashed_password: {
		type: Object,
		default: null,
	},
	isAdmin: {
		type: Number,
		default: 1,
	},
})

module.exports = mongoose.model("Users", userSchema)
