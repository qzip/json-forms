const mongoose = require("mongoose");
const FormSchema= mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    formschema: {
        type: Object,
        required: true,
    },
    uischema: {
        type: Object,
        default: null
    }
});

module.exports=mongoose.model("schema", FormSchema);