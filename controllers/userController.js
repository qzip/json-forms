const expressAsyncHandler = require("express-async-handler")
const jwt = require("jsonwebtoken")
const { FindUser } = require("../databaseAdapters/FindAdapter")
const { UpdateUser } = require("../databaseAdapters/UpdateAdapter")

exports.authentication = async (req, res) => {
	const { number, code } = req.body
	try {
		const payload = {
			user: {
				number,
				code,
			},
		}
		jwt.sign(
			payload,
			process.env.JWT_SECRET,
			{
				//set the token expiration time
				expiresIn: 36000,
			},
			(err, token) => {
				if (err) {
					throw err
				}
				res.json({ token })
			}
		)
	} catch (err) {
		res.status(500).send("Server Error")
	}
}

exports.getAllUsers = expressAsyncHandler(async (req, res) => {
	const { limit, skip } = req.query

	const users = await FindUser(null, parseInt(limit), parseInt(skip))

	res.json({ users })
})

exports.createAdmin = expressAsyncHandler(async (req, res) => {
	const userEmail = req.params.email

	let user = await FindUser(userEmail)

	user.isAdmin = 2

	user = await UpdateUser(userEmail, user)

	res.json({ status: "success" })
})

exports.removeAdmin = expressAsyncHandler(async (req, res) => {
	const userEmail = req.params.email

	let user = await FindUser(userEmail)

	user.isAdmin = 1

	user = await UpdateUser(userEmail, user)

	res.json({ status: "success" })
})

exports.getUser = expressAsyncHandler(async (req, res) => {
	const userEmail = req.params.email

	let user = await FindUser(userEmail)

	res.json({ user })
})
