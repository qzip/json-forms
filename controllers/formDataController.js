const { FindFormData } = require("../databaseAdapters/FindAdapter")
const { CreateFormData } = require("../databaseAdapters/CreateAdapter")
const expressAsyncHandler = require("express-async-handler")
const {
	UpdateUser,
	UpdateFormData,
} = require("../databaseAdapters/UpdateAdapter")

exports.createFormData = async (req, res) => {
	const { formdata, type } = req.body

	try {
		const newData = await CreateFormData({
			type: type,
			formdata: formdata,
		})

		res.send(newData)
	} catch (err) {
		res.status(500).send("Server Error")
	}
}

exports.getFormDataByType = async (req, res) => {
	try {
		let data = await FindFormData(req.params.id)
		data._id = data.id
		res.json(data)
	} catch (err) {
		res.status(500).send("Server Error")
	}
}

exports.getAllFormData = expressAsyncHandler(async (req, res) => {
	const { limit, skip } = req.query

	const formArray = await FindFormData(null, parseInt(limit), parseInt(skip))

	res.json({ formArray })
})

exports.getFormData = expressAsyncHandler(async (req, res) => {
	const form = await FindFormData(req.params.id)

	res.json({ form })
})

exports.approveFormData = expressAsyncHandler(async (req, res) => {
	let formData = await FindFormData(req.params.id)

	formData.approved = 1

	formData = await UpdateFormData(req.params.id, formData)

	res.json({ status: "success" })
})

exports.rejectFormData = expressAsyncHandler(async (req, res) => {
	let formData = await FindFormData(req.params.id)

	formData.approved = 2

	formData = await UpdateFormData(req.params.id, formData)

	res.json({ status: "success" })
})
