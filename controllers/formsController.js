const expressAsyncHandler = require("express-async-handler")
const { CreateForm } = require("../databaseAdapters/CreateAdapter")
const { DeleteForm } = require("../databaseAdapters/DatabaseAdapter")
const { FindForm } = require("../databaseAdapters/FindAdapter")
const { UpdateForm } = require("../databaseAdapters/UpdateAdapter")

exports.types = async (req, res) => {
	try {
		const forms = await FindForm()
		const formTypes = forms.map((form) => form.type)
		res.json(formTypes)
	} catch (er) {
		console.error(er)
		res.status(500).send("Server Error")
	}
}

exports.getFormByType = async (req, res) => {
	try {
		const currentform = await FindForm("type", req.params.type)
		res.json(currentform)
	} catch (er) {
		console.error(er)
		res.status(500).send("Server Error")
	}
}

exports.getAllForms = expressAsyncHandler(async (req, res) => {
	const formArray = await FindForm()
	res.json({ formArray })
})

exports.getForm = expressAsyncHandler(async (req, res) => {
	const form = await FindForm("id", req.params.id)

	res.json({ form })
})

exports.updateForm = expressAsyncHandler(async (req, res) => {
	const { type, formschema, uischema } = req.body

	let form = await FindForm("id", req.params.id)

	if (type) {
		form.type = type
	}
	if (formschema) {
		form.formschema = formschema
	}
	if (uischema) {
		form.uischema = uischema
	}

	form = await UpdateForm(form)

	res.json({ status: "success" })
})

exports.deleteForm = expressAsyncHandler(async (req, res) => {
	await DeleteForm(req.params.id)

	res.json({ status: "success" })
})

exports.createForm = expressAsyncHandler(async (req, res) => {
	await CreateForm(req.body.schema)

	res.json({ status: "success" })
})
