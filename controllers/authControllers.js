const asyncHandler = require("express-async-handler")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

const { FindUser } = require("../databaseAdapters/FindAdapter")
const { CreateUser } = require("../databaseAdapters/CreateAdapter")

exports.signup = asyncHandler(async (req, res) => {
	const { email, name, password } = req.body

	const existingUser = await FindUser("email", email)

	if (!existingUser) {
		const hashed_password = await bcrypt.hash(password, 10)

		const user = await CreateUser({ email, name, hashed_password })

		const token = jwt.sign({ user: user.email }, process.env.JWT_SECRET, {
			expiresIn: "1d",
		})

		if (!user) {
			res.json({ status: "fail", payload: "Something went wrong." })
		}
		return res.json({ token })
	} else {
		res.json({ status: "fail", payload: "Already signed up." })
	}
})

exports.signin = asyncHandler(async (req, res) => {
	const { email, password } = req.body

	const user = await FindUser(email)

	if (user) {
		const decoded = await bcrypt.compare(password, user.hashed_password)
		if (decoded) {
			const token = jwt.sign({ user: user.email }, process.env.JWT_SECRET, {
				expiresIn: "1d",
			})
			return res.json({ token })
		}
		res.json({ status: "fail", payload: "Invalid username or password." })
	}

	res.json({ status: "fail", payload: "Invalid username or password." })
})
