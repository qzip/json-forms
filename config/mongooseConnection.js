const mongoose = require("mongoose")

mongooseConnection = async () => {
	try {
		await mongoose.connect(process.env.MONGO_URI, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true,
		})
		console.log("MongoDB connected")
	} catch (er) {
		console.error(error.message)
		process.exit(1)
	}
}

module.exports = mongooseConnection
