const nano = require("nano")

const couch = new NodeCouchDb({
	auth: {
		user: "login",
		pass: "secret",
	},
	host: process.env.COUCH_DB_HOST,
})

module.exports = couch
