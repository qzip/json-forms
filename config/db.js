require("dotenv").config()
const mongooseConnection = require("./mongooseConnection")
const nano = require("nano")(`${process.env.COUCH_DB_URI}`)

let connectDB

if (process.env.DATABASE === "1") connectDB = mongooseConnection()

if (process.env.DATABASE === "2") {
	connectDB = nano
	console.log("Couch DB connected")
}

module.exports = connectDB
