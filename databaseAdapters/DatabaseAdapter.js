const Forms = require("../models/Forms")
const FormData = require("../models/FormData")
const User = require("../models/UserModel")

const connectDB = require("../config/db")
const expressAsyncHandler = require("express-async-handler")

exports.DeleteForm = expressAsyncHandler(async (value) => {
	switch (process.env.DATABASE) {
		case "1": {
			if (value) return await Forms.findByIdAndDelete(value)
			break
		}
		case "2": {
			let form = connectDB.use("form")

			data = await form
				.find({ selector: { _id: value } })
				.then((body) => body.docs[0])

			data = await form.destroy(value, data._rev)

			return data
		}
	}
})
