const Forms = require("../models/Forms")
const FormData = require("../models/FormData")
const User = require("../models/UserModel")

const connectDB = require("../config/db")

exports.FindForm = async (type, value) => {
	switch (process.env.DATABASE) {
		case "1": {
			if (!type) return await Forms.find()
			else if (type === "type") return await Forms.findOne({ type: value })
			else return await Forms.findById(value)
		}
		case "2": {
			let data

			if (type === "id") {
				let form = await connectDB.use("form")

				data = await form
					.find({ selector: { _id: value } })
					.then((body) => body.docs[0])
			} else if (type === "type") {
				let form = connectDB.use("form")

				data = await form
					.find({ selector: { type: value } })
					.then((body) => body.docs[0])
			} else if (!type) {
				let form = await connectDB.use("form")

				data = await form.list({ include_docs: true }).then((body) => body.rows)
				data = data.map((rev) => rev.doc)
			}

			return data
		}
	}
}

exports.FindFormData = async (value, limit, skip) => {
	switch (process.env.DATABASE) {
		case "1":
			{
				if (!value) return FormData.find().limit(limit).skip(skip)
				if (value) return FormData.findById(value)
			}
			break
		case "2": {
			let data

			if (value === null) {
				let formdata = await connectDB.use("form-data")

				data = await formdata
					.find({
						selector: {},
						limit,
						skip,
					})
					.then((body) => body.docs)
			} else {
				let formdata = await connectDB.use("form-data")

				data = await formdata
					.find({
						selector: {
							_id: value,
						},
					})
					.then((body) => body.docs[0])
			}

			return data
		}
	}
}

exports.FindUser = async (value, limit, skip) => {
	let data

	switch (process.env.DATABASE) {
		case "1": {
			if (!value) {
				data = await User.find().limit(limit).skip(skip)
			} else {
				data = await User.findOne({ email: value })
			}
			break
		}
		case "2": {
			let users = await connectDB.use("users")

			if (!value) {
				data = await users
					.find({
						selector: {},
						limit,
						skip,
					})
					.then((body) => body.docs)
			} else {
				data = await users
					.find({
						selector: {
							email: value,
						},
					})
					.then((body) => body.docs[0])
			}
		}
	}

	return data
}
