const User = require("../models/UserModel")
const FormData = require("../models/FormData")
const Form = require("../models/Forms")

const connectDB = require("../config/db")

exports.UpdateUser = async (value, user) => {
	let data

	switch (process.env.DATABASE) {
		case "1": {
			data = await User.findOneAndUpdate({ email: value }, user)
			break
		}
		case "2": {
			let users = await connectDB.use("users")

			data = await users.insert(user)
		}
	}

	return data
}

exports.UpdateFormData = async (value, formData) => {
	let data

	switch (process.env.DATABASE) {
		case "1": {
			data = await FormData.findOneAndUpdate({ _id: value }, formData)

			break
		}
		case "2": {
			let formdata = await connectDB.use("form-data")

			data = await formdata.insert(formData)
		}
	}

	return data
}

exports.UpdateForm = async (formValue) => {
	let data

	switch (process.env.DATABASE) {
		case "1": {
			data = await Form.findOneAndUpdate({ _id: formValue._id }, formValue)
			break
		}
		case "2": {
			let form = await connectDB.use("form")

			data = await form.insert(formValue).catch((err) => console.log(err))
		}
	}

	return data
}
