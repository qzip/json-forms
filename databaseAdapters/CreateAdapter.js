const Forms = require("../models/Forms")
const FormData = require("../models/FormData")

const connectDB = require("../config/db")
const UserModel = require("../models/UserModel")

exports.CreateFormData = async (form) => {
	switch (process.env.DATABASE) {
		case "1": {
			const formAnswer = await FormData.create(form)
			return formAnswer
		}
		case "2": {
			const formData = await connectDB.use("form-data")

			let formAnswer = await formData.insert(form)

			return formAnswer
		}
	}
}

exports.CreateUser = async (user) => {
	switch (process.env.DATABASE) {
		case "1": {
			const userAnswer = await UserModel.create(user)
			return userAnswer
		}
		case "2": {
			const users = await connectDB.use("users")

			let userAnswer = await users.insert(user)

			return userAnswer
		}
	}
}

exports.CreateForm = async (form) => {
	switch (process.env.DATABASE) {
		case "1": {
			const formAnswer = await Forms.create(form)
			return formAnswer
		}
		case "2": {
			const formSchema = await connectDB.use("form")

			let formAnswer = await formSchema.insert(form)

			return formAnswer
		}
	}
}
