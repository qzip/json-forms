import { BrowserRouter, Switch, Route } from "react-router-dom"
import Homepage from "./components/pages/Homepage"
import SignIn from "./components/pages/SignIn"
import SignUp from "./components/pages/SignUp"

function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/" exact component={SignIn} />
				<Route path="/signup" exact component={SignUp} />
				<Route path="/home" component={Homepage} />
			</Switch>
		</BrowserRouter>
	)
}

export default App
