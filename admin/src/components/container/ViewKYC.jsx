import React, { useState, useEffect } from "react"
import fetchCall from "../../utils"
import { makeStyles } from "@material-ui/core/styles"
import { Container, Grid, Typography, Paper, Button } from "@material-ui/core"
import { Link } from "react-router-dom"

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: "2rem 1rem",
		flexWrap: "wrap",
	},
}))

const ViewKYC = () => {
	const [kycList, setKYCList] = useState([])
	const limit = 5

	const classes = useStyles()

	const token = JSON.parse(localStorage.getItem("token"))
	const [pageNum, setPageNum] = useState(0)

	useEffect(() => {
		const fetchUsers = async () => {
			await fetchCall(
				`formdata?limit=${limit}&skip=${limit * pageNum}`,
				"GET",
				token
			).then((res) => setKYCList(res.formArray))
		}
		fetchUsers()
	}, [pageNum])

	return (
		<div className="container">
			<Container>
				<Typography variant="h2">KYC List</Typography>
				<Grid container spacing={2} xs={12}>
					{kycList.length > 0
						? kycList.map((formData) => (
								<Grid item xs={4}>
									<Link className="link" to={`/home/view-kyc/${formData._id}`}>
										<Paper className={classes.paper}>
											<Typography variant="h4">
												{formData.formdata && formData.formdata.fullname}
											</Typography>
											<Typography variant="h6">
												{formData.formdata && formData.type}
											</Typography>
										</Paper>
									</Link>
								</Grid>
						  ))
						: null}
					<Grid container item justify="space-around">
						<Grid item>
							<Button
								variant="contained"
								color="secondary"
								onClick={() => {
									setPageNum(pageNum - 1)
								}}
								disabled={pageNum === 0 ? true : false}
							>
								Previous
							</Button>
						</Grid>
						<Grid item>
							<Button
								variant="contained"
								color="primary"
								onClick={() => {
									setPageNum(pageNum + 1)
								}}
							>
								Next
							</Button>
						</Grid>
					</Grid>
				</Grid>
			</Container>
		</div>
	)
}

export default ViewKYC
