import React, { useState, useEffect } from "react"
import fetchCall from "../../utils"
import { makeStyles } from "@material-ui/core/styles"
import { Container, Grid, Typography, Paper, Button } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: "2rem 1rem",
		flexWrap: "wrap",
	},
}))

const UserList = () => {
	const classes = useStyles()
	const limit = 5

	const [userList, setUserList] = useState([])
	const [flag, setFlag] = useState(true)

	const token = JSON.parse(localStorage.getItem("token"))
	const [pageNum, setPageNum] = useState(0)

	useEffect(() => {
		if (flag) {
			const fetchUsers = async () => {
				await fetchCall(
					`user/users?limit=${limit}&skip=${pageNum * limit}`,
					"GET",
					token
				).then((res) => {
					setUserList(res.users)
					setFlag(false)
					console.log(res)
				})
			}
			fetchUsers()
		}
	}, [token, flag, pageNum])

	const makeAdmin = async (user) => {
		const data = await fetchCall(`user/${user}`, "POST", token)

		if (data.status === "success") {
			setFlag(true)
		}
	}

	const removeAdmin = async (user) => {
		const data = await fetchCall(`user/${user}`, "PUT", token)

		if (data.status === "success") {
			setFlag(true)
		}
	}

	return (
		<div className="container">
			<Container>
				<Typography variant="h2">User List</Typography>
				<Grid container spacing={2} xs={12}>
					{userList.map((user) => (
						<Grid item xs={12} md={6}>
							<Paper className={classes.paper}>
								<Typography variant="h4">{user.name}</Typography>
								<Typography variant="h5">{user.email}</Typography>
								<Grid container xs={12}>
									<Grid item xs={12}>
										{user.isAdmin === 1 ? (
											<Button
												variant="contained"
												color="primary"
												onClick={() => makeAdmin(user.email)}
											>
												Make Admin
											</Button>
										) : (
											<Button
												variant="contained"
												color="secondary"
												onClick={() => removeAdmin(user.email)}
											>
												Remove
											</Button>
										)}
									</Grid>
								</Grid>
							</Paper>
						</Grid>
					))}
					<Grid container item justify="space-around">
						<Grid item>
							<Button
								variant="contained"
								color="secondary"
								onClick={() => {
									setPageNum(pageNum - 1)
									setFlag(true)
								}}
								disabled={pageNum === 0 ? true : false}
							>
								Previous
							</Button>
						</Grid>
						<Grid item>
							<Button
								variant="contained"
								color="primary"
								onClick={() => {
									setPageNum(pageNum + 1)
									setFlag(true)
								}}
							>
								Next
							</Button>
						</Grid>
					</Grid>
				</Grid>
			</Container>
		</div>
	)
}

export default UserList
