import React, { useState, useEffect } from "react"
import { makeStyles } from "@material-ui/core/styles"
import fetchCall from "../../utils"
import { Container, Grid, Typography, Paper, Button } from "@material-ui/core"
import { Link } from "react-router-dom"

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: "2rem 1rem",
		flexWrap: "wrap",
	},
}))

const JSONForm = () => {
	const classes = useStyles()

	const [formList, setFormList] = useState([])

	const token = JSON.parse(localStorage.getItem("token"))

	useEffect(() => {
		const fetchUsers = async () => {
			await fetchCall("forms", "GET", token).then((res) => {
				setFormList(res.formArray)
			})
		}
		fetchUsers()
	}, [])

	return (
		<div className="container">
			<Container>
				<Typography variant="h2">KYC List</Typography>
				<Grid container spacing={2} xs={12}>
					{formList[0] &&
						formList.map((formData) => (
							<Grid item xs={12} md={6}>
								<Link className="link" to={`/home/json-forms/${formData._id}`}>
									<Paper className={classes.paper}>
										<Typography variant="h4">{formData.type}</Typography>
									</Paper>
								</Link>
							</Grid>
						))}
					<Grid item>
						<Link className="link" to="/home/json-forms/add">
							<Button variant="contained" color="primary">
								Add a Form
							</Button>
						</Link>
					</Grid>
				</Grid>
			</Container>
		</div>
	)
}

export default JSONForm
