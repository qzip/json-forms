import React from "react"
import { Route, Switch } from "react-router-dom"

import UserList from "../container/UserList"
import ViewKYC from "../container/ViewKYC"
import JSONForm from "../container/JSONForm"
import KYCPage from "../pages/KYCPage"
import FormPage from "../pages/FormPage"
import CreateForm from "../pages/CreateForm"

class Routes extends React.Component {
	render() {
		return (
			<Switch>
				<Route exact path="/home" component={UserList} />
				<Route exact path="/home/view-kyc" component={ViewKYC} />
				<Route exact path="/home/json-forms" component={JSONForm} />
				<Route exact path="/home/view-kyc/:kycId" component={KYCPage} />
				<Route exact path="/home/json-forms/add" component={CreateForm} />
				<Route exact path="/home/json-forms/:formId" component={FormPage} />
			</Switch>
		)
	}
}

export default Routes
