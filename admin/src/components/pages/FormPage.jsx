import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import {
	Container,
	Grid,
	TextField,
	Button,
	TextareaAutosize,
} from "@material-ui/core"
import Form from "react-jsonschema-form"
import fetchCall from "../../utils"
import { Alert } from "react-bootstrap"

const FormPage = () => {
	const { formId } = useParams()
	const [formData, setFormData] = useState()
	const [formSchema, setFormSchema] = useState()
	const [flag, setFlag] = useState(true)
	const [stringSchema, setStringSchema] = useState()

	const [success, setSuccess] = useState("")

	const token = JSON.parse(localStorage.getItem("token"))

	useEffect(() => {
		if (flag) {
			const fetchFormData = async () => {
				const data = await fetchCall(`forms/${formId}`, "POST", token)

				setFormData(data.form)
				setFormSchema(data.form.formschema)
				setStringSchema(JSON.stringify(data.form.formschema, undefined, 4))
				setFlag(false)
			}
			fetchFormData()
		}
	}, [flag])

	const handlePreview = () => {
		setFormSchema(JSON.parse(stringSchema))
	}

	const handleSubmit = async () => {
		const data = await fetchCall(`forms/${formId}`, "PUT", token, {
			formschema: formSchema,
		})
		if (data.status === "success") {
			setSuccess("Updated.")
			setTimeout(() => {
				setSuccess("")
			}, 3000)
		}
	}

	const handleDelete = async () => {
		const data = await fetchCall(`forms/${formId}`, "DELETE", token)
		if (data.status === "success") {
			setSuccess("Deleted")
			setTimeout(() => {
				setSuccess("")
			}, 3000)
		}
	}

	return (
		<div className="container">
			<Alert>{success}</Alert>
			<Container>
				{formSchema && (
					<Form
						className="form-container"
						schema={formSchema}
						// onSubmit={onSubmit}
					></Form>
				)}

				{/* {JSON.stringify(formData.formschema, undefined, 4)} */}
				<TextareaAutosize
					onChange={(e) => {
						setStringSchema(e.target.value, undefined, 4)
					}}
					value={stringSchema}
				></TextareaAutosize>
				<Alert>{success}</Alert>
				<Button onClick={handlePreview} variant="contained" color="primary">
					Preview
				</Button>
				<Button onClick={handleSubmit} variant="contained" color="primary">
					Submit
				</Button>
				<Button onClick={handleDelete} variant="contained" color="secondary">
					Delete
				</Button>
			</Container>
		</div>
	)
}

export default FormPage
