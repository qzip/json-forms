import React, { useEffect, useState } from "react"
import { Container, Grid, TextField, Button } from "@material-ui/core"
import * as ReactBootStrap from "react-bootstrap"
import fetchCall from "../../utils"
import { useParams } from "react-router-dom"

const KYCPage = () => {
	const [formData, setFormData] = useState()
	const [flag, setFlag] = useState(true)

	const { kycId } = useParams()

	const token = JSON.parse(localStorage.getItem("token"))

	useEffect(() => {
		if (flag) {
			const fetchFormData = async () => {
				const data = await fetchCall(`formdata/${kycId}`, "GET", token)
				setFormData(data)
				setFlag(false)
			}
			fetchFormData()
		}
	}, [flag])

	const accept = async () => {
		const data = await fetchCall(`formdata/${kycId}`, "POST", token)
		if (data.status === "success") {
			setFlag(true)
		}
	}

	const reject = async () => {
		const data = await fetchCall(`formdata/${kycId}`, "PUT", token)
		if (data.status === "success") {
			setFlag(true)
		}
	}

	return (
		<div className="container">
			{formData && (
				<Container>
					<Grid container spacing={4} direction="column">
						<Grid container xs={12} item direction="row">
							<Grid item xs={6}>
								<Button
									variant="contained"
									color="primary"
									onClick={() => accept()}
									disabled={
										!formData.approved
											? false
											: formData.approved === 0
											? false
											: formData.approved === 2
											? false
											: true
									}
								>
									Accept
								</Button>
							</Grid>
							<Grid item xs={6}>
								<Button
									disabled={
										!formData.approved
											? false
											: formData.approved === 0
											? false
											: formData.approved === 1
											? false
											: true
									}
									variant="contained"
									color="secondary"
									onClick={() => reject()}
								>
									Reject
								</Button>
							</Grid>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="DOB"
								variant="outlined"
								value={formData.formdata.Dob}
								disabled="true"
								size="medium"
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="Address"
								variant="outlined"
								value={formData.formdata.address}
								disabled="true"
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="Country"
								variant="outlined"
								value={formData.formdata.country}
								disabled="true"
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="Doc-Type"
								variant="outlined"
								value={formData.formdata.doctype}
								disabled="true"
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="Full Name"
								variant="outlined"
								value={formData.formdata.fullname}
								disabled="true"
							/>
						</Grid>
						<Grid item xs={12}>
							<img src={formData.formdata.file} />
						</Grid>

						{/* <ReactBootStrap.Table>
							<tbody>
								{formData.formData &&
									Object.keys(formData.formData).map((key) => (
										<tr key={key}>
											<td>{key.charAt(0).toUpperCase() + key.slice(1)}</td>
											{key === "file" ? (
												<td>
													<img
														src={formData.formData[key]}
														style={{ width: "55%", height: "70%" }}
														alt="file data"
													></img>
												</td>
											) : (
												<td>{formData.formData[key]}</td>
											)}
										</tr>
									))}
							</tbody>
						</ReactBootStrap.Table> */}
					</Grid>
				</Container>
			)}
		</div>
	)
}

export default KYCPage
