import React, { useState } from "react"
import {
	Button,
	Container,
	Grid,
	TextareaAutosize,
	TextField,
} from "@material-ui/core"
import fetchCall from "../../utils"
import { Alert } from "react-bootstrap"
import Form from "react-jsonschema-form"

const CreateForm = () => {
	const token = JSON.parse(localStorage.getItem("token"))

	const [type, setType] = useState("")
	const [success, setSuccess] = useState("")
	const [stringSchema, setStringSchema] = useState("")
	const [schema, setSchema] = useState({})

	const handlePreview = () => {
		setSchema(JSON.parse(stringSchema))
	}

	const handleSubmit = async () => {
		const data = await fetchCall(`forms`, "POST", token, {
			schema: {
				type,
				formschema: schema,
			},
		})

		if (data.status === "success") {
			setSuccess("Created")
		}
	}

	return (
		<div className="container">
			<Container>
				<Grid container direction="column" spacing={4}>
					<Grid item>
						<TextField
							onChange={(e) => setType(e.target.value)}
							label="Type"
							variant="outlined"
						/>
					</Grid>
					<Grid item>
						<TextareaAutosize
							value={stringSchema}
							onChange={(e) => setStringSchema(e.target.value, undefined, 4)}
							rowsMin={30}
						/>
					</Grid>
					<Grid item>
						<Button onClick={handlePreview} color="primary" variant="contained">
							Preview
						</Button>
					</Grid>
					<Grid item>
						{stringSchema && (
							<Form className="form-container" schema={schema}></Form>
						)}
					</Grid>
					<Grid item>
						<Alert>{success}</Alert>
					</Grid>
					<Grid item>
						<Button onClick={handleSubmit} color="primary" variant="contained">
							Submit
						</Button>
					</Grid>
				</Grid>
			</Container>
		</div>
	)
}

export default CreateForm
