JSONForm-kyc-POC
Proof of Concept of JSONForm Schema built of KYC
Open Sourced under Apache License by QzIP Blockchain Technology LLP
Preview : https://immense-hollows-74804.herokuapp.com/

Install Dependencies
npm install

Mongo connection setup
Edit .env file to include MONGO_URI variable for your MongoDB URI

Authentication setup
Edit /client/firebase.js to include the correct script

You can view the Client side at https://json-form-kyc.herokuapp.com/ and the Admin side at https://hardcore-bhaskara-a7362b.netlify.app/. You can login as yash@gmail.com with the password -> password to view the already submitted forms.

Configure REACT_APP_DATABASE in .env as 1 for firebase and 2 for your own backend database.


