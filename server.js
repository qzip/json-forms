const express = require("express")
const connectDB = require("./config/db")
const dotenv = require("dotenv")
const morgan = require("morgan")

const path = require("path")

dotenv.config()

const app = express()

//init middleware
app.use(express.json({ extended: false }))
app.use(morgan("dev"))

//define routes

app.use("/api/forms", require("./routes/forms"))
app.use("/api/user", require("./routes/users"))
app.use("/api/formdata", require("./routes/formdata"))
app.use("/api/auth", require("./routes/authRoutes"))

// Serve static assets in production
if (process.env.NODE_ENV === "production") {
	// Set static folder
	// app.use(express.static('client/build'));
	app.use(express.static(path.join(__dirname, "./client/build")))

	app.get("/*", (req, res) =>
		// res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
		res.sendFile(path.join(__dirname + "./client/build/index.html"))
	)
}

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log(`server connected at port ${PORT}`))
